// SoundManager2 Settings
soundManager.setup({
    url: '/swf',
    flashVersion: 9,
    debugMode: true,
    preferFlash: false, // prefer 100% HTML5 mode, where both supported
    onready: function() {
        console.log('SM2 ready!');
    },
    ontimeout: function() {
        console.log('SM2 init failed!');
    },
    defaultOptions: {
        // set global default volume for all sound objects
        volume: 70
    }
});
this.config = {
    flashVersion: 9, // version of Flash to tell SoundManager to use - either 8 or 9.
    allowRightClick: false, // let users right-click MP3 links ("save as...", etc.) or discourage (can't prevent.)
    useThrottling: false, // try to rate-limit potentially-expensive calls (eg. dragging position around)
    autoStart: false, // begin playing first sound when page loads
    playNext: true, // stop after one sound, or play through list until end
    updatePageTitle: true, // change the page title while playing sounds
    emptyTime: '-:--' // null/undefined timer values (before data is available)
}
// Initialize the PubNub API connection.
var pubnub = PUBNUB.init({
    publish_key: 'pub-c-deeae58d-457f-4d34-9741-3908819a7e52',
    subscribe_key: 'sub-c-d63f5f06-5914-11e3-91aa-02ee2ddab7fe'
});
// Pubnub Chat Configuration
$(document).ready(function() {
    // Grab references for all of our elements.
    var messageContent = $('#messageContent'),
        sendMessageButton = $('#sendMessageButton'),
        messageList = $('#messageList');
    // Handles all the messages coming in from pubnub.subscribe.
    function handleMessage(message) {
        var messageEl = $("<li class='message'>" + "<span class='username'>" + message.username + ": </span>" + message.text + "</li>");
        messageList.append(messageEl);
        // Scroll to bottom of page
        $("#messageList").animate({
            scrollTop: $("#messageList").prop("scrollHeight")
        }, 'slow');
    };
    // Compose and send a message when the user clicks our send message button.
    sendMessageButton.click(function(event) {
        var message = messageContent.val();
        if (message != '') {
            pubnub.publish({
                channel: 'chat',
                message: {
                    username: window.username,
                    text: message
                }
            });
            messageContent.val("");
        }
    });
    // Also send a message when the user hits the enter button in the text area.
    messageContent.bind('keydown', function(event) {
        if ((event.keyCode || event.charCode) !== 13) return true;
        sendMessageButton.click();
        return false;
    });
    // Subscribe to messages coming in from the channel.
    pubnub.subscribe({
        channel: 'chat',
        message: handleMessage
    });
});
// Soundcloud API
$(document).ready(function() {
    SC.initialize({
        client_id: "a4206f816a58710e5c84ab6af01f346f"
    });
    $(".add-to-playlist").click(function() {
        var url = $("#soundcloud-url-input").val();
        var $btn = $(this);
        $btn.button('loading');
        // console.log(trackid);
        if (url == "") {
            alert("Fuck enter some shit");
            $btn.button('reset'); // reset the button back
        } else {
            // Get track-id using the url
            $.getJSON("http://api.soundcloud.com/resolve.json", {
                url: url,
                client_id: "a4206f816a58710e5c84ab6af01f346f"
            }, function(data) {
                var trackinfo = new Array();
                trackinfo[0] = data.id;
                trackinfo[1] = data.title;
                trackinfo[2] = data.stream_url;
                trackinfo[3] = localStorage.getItem("safetime");
                // Add song to playlist-ui
                pubnub.publish({
                    channel: "playlist-soundcloud",
                    message: trackinfo
                });
            });
        };
    });
});
// Player Controls
$(document).ready(function() {
    // Previous button click handler
    $(".prev").on('click', function() {
        // console.log("clicked for previous song");        
        pubnub.publish({
            channel: "prev",
            message: localStorage.getItem("timetoplay")
        });
    });
    // Play button click handler
    $(".play, .pause").on('click', function() {
        // console.log("play something");
        pubnub.publish({
            channel: "play",
            message: localStorage.getItem("timetoplay")
        });
    });
    // Next button click handler
    $(".next").on('click', function() {
        // console.log("clicked for next song");
        pubnub.publish({
            channel: "next",
            message: localStorage.getItem("timetoplay")
        });
    });
});
// Test functions
$(document).ready(function() {
    // Test Functions
    // upload button emulation
    $(".upload-local").click(function() {
        $('#input').click();
    });
});
// Client Time
function now() {
    return +new Date;
}
// Updating the lag and storing in localStorage
function thelag() {
    start = now(); // determine the local time (in milliseconds)
    pubnub.time(function(pubnubtime) { // Getting pubnub rountrip
        lag = now() - start; // roundtrip time
        safetime = Math.round(pubnubtime/10000) + lag + 200;
        localStorage.setItem("timetoplay", safetime);
        setTimeout(function() {
            thelag();
        }, 10000);
        // console.log("pubnubtime: "+Math.round(pubnubtime/10000));
        // console.log("mytime: "+Math.round(now()));
    });
}
// Keep getting timetoplay
(function() {
    thelag();
})();

function mylag(){
    console.log(localStorage.getItem("timetoplay"));
}